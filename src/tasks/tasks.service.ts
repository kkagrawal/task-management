import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { TaskRepository } from './task.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { TaskEntity } from './task.entity';
import { TaskStatus } from './task-status.enum';
import { User } from 'src/auth/user.entity';

@Injectable()
export class TasksService {


  constructor(
    @InjectRepository(TaskRepository)
    private baseRepository: TaskRepository,
  ) { }


  getTasks(filterDto: GetTasksFilterDto, user: User, allTasks: boolean = false): Promise<TaskEntity[]> {
    return this.baseRepository.getTasks(filterDto, user, allTasks);
  }


  async getTaskById(id: number, user: User): Promise<TaskEntity> {
    const found = await this.baseRepository.findOne({where: {id, userId: user.id}});

    if (!found) {
      throw new NotFoundException(`The task with id '${id}' was not found`);
    }
    return found;

  }


  createTask(createTaskDto: CreateTaskDto, user: User): Promise<TaskEntity> {
    return this.baseRepository.creatTask(createTaskDto, user);
  }


  async updateTaskStatus(id: number, status: TaskStatus, user: User): Promise<TaskEntity> {
    let task = await this.getTaskById(id, user);
    task.status = status;
    task.save();
    return task;
  }


  async deleteTaskById(id: number): Promise<void> {
    const result = await this.baseRepository.delete(id);
    
    if(result.affected === 0) {
      throw new NotFoundException(`The task with id '${id}' was not found`);
    }
  }

  // deleteTaskById(id: string): Task {
  //   let index = this.tasks.findIndex(task => task.id == id);
  //   return this.tasks.splice(index)[0];
  // }


}
