import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Patch,
  Query,
  UsePipes,
  ValidationPipe,
  ParseIntPipe,
  UseGuards,
} from '@nestjs/common';
import { TasksService } from './tasks.service';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { TaskStatusValidationPipe } from './pipes/task-status-validation.pipe';
import { TaskEntity } from './task.entity';
import { TaskStatus } from './task-status.enum';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from 'src/auth/get-user.decorator';
import { User } from 'src/auth/user.entity';

@Controller('tasks')
@UseGuards(AuthGuard())
export class TasksController {
  constructor(private baseService: TasksService) {}

  @Get()
  getTask(
    @Query() filterDto: GetTasksFilterDto,
    @GetUser() user: User
  ): Promise<TaskEntity[]> {
    return this.baseService.getTasks(filterDto, user);
  }

  
  @Get('/all')
  getAllTask(
    @Query() filterDto: GetTasksFilterDto,
    @GetUser() user: User
  ): Promise<TaskEntity[]> {
    return this.baseService.getTasks(filterDto, user, true);
  }


  @Get('/:id')
  getTaskById(
    @Param('id', ParseIntPipe) id: number, @GetUser() user: User): Promise<TaskEntity> {
    return this.baseService.getTaskById(id, user);
  }

  @Post()
  @UsePipes(ValidationPipe)
  createTask(
    @Body() createTaskDto: CreateTaskDto,
    @GetUser() user: User
  ): Promise<TaskEntity> {
    return this.baseService.createTask(createTaskDto, user);
  }

  @Patch('/:id/status')
  updateTaskStatus(
    @Param('id', ParseIntPipe) id: number,
    @Body('status', TaskStatusValidationPipe) status: TaskStatus,
    @GetUser() user: User
  ): Promise<TaskEntity> {
    return this.baseService.updateTaskStatus(id, status, user);
  }

  @Delete('/:id')
  async deleteTask(@Param('id', ParseIntPipe) id: number): Promise<void> {
    await this.baseService.deleteTaskById(id);
  }
}
