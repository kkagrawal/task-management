import { TaskStatus } from "../task-status.enum";

export class GetTasksFilterDto {
  search: string;
  status: TaskStatus
}