import { TypeOrmModuleOptions } from '@nestjs/typeorm'

export const typeOrmConfig: TypeOrmModuleOptions = {
  type: 'mysql',
  host: 'localhost',
  port: 3306,
  username: 'root',
  password: '',
  database: 'test_taskmanagement',
  entities: [__dirname + './../**/*.entities.ts', __dirname + '/../../dist/**/*.entity.js'],
  synchronize: true
}